/*

based on the : 
https://github.com/gbenison/png-text-embed
Gregory C Benison

---------------- licence of original code ---------------
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
------------original readme ---------------------------------------------------
The utility 'png-text-append' inserts "text chunks" into PNG files.
*Note: this does _not_ mean 'drawing text on the image' in any way;
the appearance of the image will be unchanged.  This means inserting
an ancillary text chunk into the PNG stream, which can be used for 
example, to hold copyright information.

The utility 'png-text-dump' displays all text chunks present in a PNG 
file.

References:
http://stackoverflow.com/questions/9036152/insert-a-text-chunk-into-a-png-image

--------------------------------------------------------
writes one  text chunk into existing png file 
it uses standard streames so :
* one do not have to open it and close
* use fprintf(stderr) not printf 

-------------- git -------------------------------
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/png-text-append.git
git add .
git commit -m "aaa"
git push -u origin master


--------------

read_png_file(argv[1]);
process_png_file();
write_png_file(argv[2]);

---------- use -----------------------------------
to compile : 
gcc png-text-append.c -Wall
but gives warnings 


gcc png-text-append.c 
./a.out


run : 
./a.out key content < inputfile > outputfile
./a.out Comment asaasasa <r.png >r7.png 

test : 
pngcheck -t r7.png


*/
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/*
source:
http://www.w3.org/TR/PNG/#D-CRCAppendix

 the CRC (Cyclic Redundancy Check) employed in PNG chunks.
*/

   /* Table of CRCs of all 8-bit messages. */
   unsigned long crc_table[256];
   
   /* Flag: has the table been computed? Initially false. */
   int crc_table_computed = 0;
   
   /* Make the table for a fast CRC. */
   void make_crc_table(void)
   {
     unsigned long c;
     int n, k;
   
     for (n = 0; n < 256; n++) {
       c = (unsigned long) n;
       for (k = 0; k < 8; k++) {
         if (c & 1)
           c = 0xedb88320L ^ (c >> 1);
         else
           c = c >> 1;
       }
       crc_table[n] = c;
     }
     crc_table_computed = 1;
   }
  

   /* Update a running CRC with the bytes buf[0..len-1]--the CRC
      should be initialized to all 1's, and the transmitted value
      is the 1's complement of the final running CRC (see the
      crc() routine below). */
   
   unsigned long update_crc(unsigned long crc, unsigned char *buf,
                            int len)
   {
     unsigned long c = crc;
     int n;
   
     if (!crc_table_computed)
       make_crc_table();
     for (n = 0; n < len; n++) {
       c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
     }
     return c;
   }
   
   /* Return the CRC of the bytes buf[0..len-1]. */
   unsigned long crc_calculate(unsigned char *buf, int len)
   {
     return update_crc(0xffffffffL, buf, len) ^ 0xffffffffL;
   }


// end of source from w3org -------------------------
// extern unsigned long crc_calculate(char*, int);



// A PNG file consists of a PNG signature followed by a series of chunks.
// PNG file signature = The first eight bytes of a PNG file always contain the following (decimal) values:
/*
The first eight bytes of a PNG file always contain the following values:

   (decimal)              137  80  78  71  13  10  26  10
   (hexadecimal)           89  50  4e  47  0d  0a  1a  0a
   (ASCII C notation)    \211   P   N   G  \r  \n \032 \n
*/


#define PNG_SIG_SIZE 8  
const char* png_sig = "\211\120\116\107\015\012\032\012";

int little_endian = 0;

static void
endian_swap(uint32_t* x)
{
  if (little_endian)
    {
      char* buf = (char*)x;
      char tmp;
      tmp = buf[0]; buf[0]=buf[3]; buf[3] = tmp;
      tmp = buf[1]; buf[1]=buf[2]; buf[2] = tmp;
    }
}

/*



key	        Short (one line) title or caption for image
---------------------------------------------------------
Author		Name of image's creator
Description	Description of image (possibly long)
Copyright	Copyright notice
Creation Time	Time of original image creation
Software	Software used to create the image
Disclaimer	Legal disclaimer
Warning		Warning of nature of content
Source		Device used to create the image
Comment		Miscellaneous comment



Each tEXt chunk contains a keyword and a text string, in the format:

Keyword	1-79 bytes (character string)
Null separator	1 byte (null character)
Text string	0 or more bytes (character string)


*/





static void
inject_text_chunk(char *key, char *content, FILE *stream)
{
  uint32_t length = strlen(key) + 1 + strlen(content);
  char *buffer = malloc(length + 4 + 1); // text to append into outfile 
  
  sprintf(buffer, "tEXt%s", key);
  sprintf(buffer + 4 + strlen(key) + 1, "%s", content);

  //fprintf(stderr, "chunk =  %s\n", buffer);

  uint32_t length_out = length;
  endian_swap(&length_out);
  fwrite(&length_out, 1, 4, stream);
  fwrite(buffer, 1, length + 4, stream);
  
  /* calculate and write CRC sum */
  uint32_t crc_out =   crc_calculate(buffer, length + 4);
  endian_swap(&crc_out);
  fwrite(&crc_out, 1, 4, stream);
}

int
main(int argc, char *argv[])
{
  if (argc < 3) {

    fprintf(stderr, "argc < 3\n");
    fprintf(stderr, "Usage: %s <key> <content> <inputfile >outputfile\n", argv[0]);
    fprintf(stderr, "Reads a PNG image from standard input\n");
    fprintf(stderr, "injects one text chunk (key=content)\n");
    fprintf(stderr, "copies all other chunks unchanged to standard output\n");
    
  }

  

  
  /* is this a little-endian machine? */
  {
    uint32_t endian_test = 1;
    little_endian = ((*(char*)(&endian_test)) != 0);
     
  }

  FILE *infile = stdin;
  FILE *outfile = stdout;

  int buf_size = 1024;
  char *buffer = malloc(buf_size);


  // png signature 
  char sig[PNG_SIG_SIZE];
  assert (fread(sig, 1, PNG_SIG_SIZE, infile) == PNG_SIG_SIZE);
  assert (strncmp(png_sig, sig, PNG_SIG_SIZE) == 0);
  fwrite(png_sig, 1, PNG_SIG_SIZE, outfile);

  
  // chunks
  int did_text_chunk = 0; // boolean value

  while(1) {
    uint32_t length;
    char name[5]={'\0'}; // http://stackoverflow.com/questions/15577124/strange-character-after-an-array-of-characters
    uint32_t crc;
    
    int n_read_length = fread(&length, 1, 4, infile);
    if (n_read_length != 4) 
        break; // exit from while loop
    
    endian_swap(&length);
    fread(name, 1, 4, infile);
    
    //fprintf(stderr, "input chunk name = %s\n", name ); 
 
    /* make sure buffer is large enough to hold contents */
    while (buf_size < length) {
      buf_size *= 2;
      buffer = realloc(buffer, buf_size);
    }

    fread(buffer, length, 1, infile);
    fread(&crc, 1, 4, infile);
    endian_swap(&crc);

    uint32_t length_out = length;
    endian_swap(&length_out);
    uint32_t crc_out = crc;
    endian_swap(&crc_out);


    //fprintf(stderr, "name = %s ; strcmp(name, \"IDAT\") = %d ; strcmp(name, \"IEND\") = %d ; did_text_chunk = %d \n", name, strcmp(name, "IDAT"), strcmp(name, "IEND"), did_text_chunk );
    /* text is injected just before first IDAT or IEND chunk */
    if (((!strncmp(name, "IDAT", 4)) || (!strncmp(name,"IEND", 4))) && (!did_text_chunk) )
      //{
	// if (!did_text_chunk) 
        {
	  inject_text_chunk(argv[1],argv[2],outfile);
          //fprintf(stderr, "infile chunk name = %s ; chunk =  %s.%s injected into outfile\n",name, argv[1], argv[2]);
          did_text_chunk = 1;
	}

      //} 
     else{
    /* echo chunks to output */
    fwrite(&length_out, 1, 4, outfile);
    fwrite(name, 1, 4, outfile);
    fwrite(buffer, 1, length, outfile);
    fwrite(&crc_out, 1, 4, outfile);
    //fprintf(stderr, "chunk %s copied from input to output file did_text_chunk = %d\n", name, did_text_chunk);
    }
  } // while


  fprintf(stderr, "file saved \n");
  return(0);
}

